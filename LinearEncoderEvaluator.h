/*
 * Copyright 2013 Li-Yuchong(李雨翀) <93@liyc.me>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR AND CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef __LINEAR_ENCODER_EVALUATOR_H__
#define __LINEAR_ENCODER_EVALUATOR_H__

#include <assert.h>
#include <float.h>
#include <math.h>

#include "Types.h"

struct AbsoluteEncoderParameters
{
    float a, b;                  /* f(x) = ax + b */
};

struct LinearEncoderEvaluator
{
    uint32_t id;                 /* maybe somebody needs an ID... */
    
    uint8_t polePairNumber;      /* pole pair number of the motor */
    float   phaseDiff;           /* the angle difference bwtween the encoder
                                  * (mechanical angle)and electric
                                  * angle in the algorithm */
};

/* estimate values a, b to fit domain [lower, upper].
 * INL(Integral Non Linearity) will be calculated and set, if not NULL. */
struct AbsoluteEncoderParameters*
estimateAbsoluteEncoderParas(const float *encoder, const uint32_t sampleLength,
                             const float lower,    const float upper,
                             float *INL, /* Integral Non Linearity */
                             struct AbsoluteEncoderParameters *calibrator);

/* calculate mechanical angle given the parameter of the encoder and the 
 * sampled value. */
float calcuAbsoluteEncoderAngle(const struct AbsoluteEncoderParameters *encoderParas,
                                const float sampledValue);

/* linear encoder calibrator & parameters estimating algorithm. */
struct LinearEncoderEvaluator *
estimateEvaluatorParameters(const float *encoder,        const float *elecAngle,
                            const uint32_t sampleLength, const uint32_t roundEA,
                            struct LinearEncoderEvaluator *evaluator);

float estimateElectricAngle(struct LinearEncoderEvaluator *evaluator,
                            const float encoderAngle);

#endif /* __LINEAR_ENCODER_EVALUATOR_H__ */
