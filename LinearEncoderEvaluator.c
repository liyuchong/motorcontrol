/*
 * Copyright 2013 Li-Yuchong(李雨翀) <93@liyc.me>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR AND CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "LinearEncoderEvaluator.h"

static const float *findMinMax(const float *arr, const uint32_t length,
                               float *min, float *max)
{
    uint32_t i;
    
    *max = -FLT_MAX;
    *min =  FLT_MAX;
    
    for(i = 0; i < length; i++)
    {
        if(arr[i] > *max) *max = arr[i];
        if(arr[i] < *min) *min = arr[i];
    }
    
    return arr;
}

/* extremum finder using differentiation method */
static bool derivativeCalcExtremum(const float thres,    const float *data,
                                   const uint32_t start, const uint32_t length,
                                   uint32_t *index)
{
    uint32_t i;
    for(i = start + 2; i < length - 2; i++)
    {
        /* calculate derivative */
        float diff =
        0.125f * (data[i - 2] - 4.0f * data[i - 1] + 3.0f * data[i + 2]);
        
        if(fabsf(diff) > thres)
        {
            *index = i;
            return true;  /* success: extremum found */
        }
    }
    
    return false;  /* unable to find extremum */
}

struct LinearEncoderEvaluator *
estimateEvaluatorParameters(const float *encoder,        const float *elecAngle,
                            const uint32_t sampleLength, const uint32_t roundEA,
                            struct LinearEncoderEvaluator *evaluator)
{
    const uint32_t halfRoundEA = roundEA >> 1;
    uint32_t i;
    uint32_t ext1, ext2; /* the 1st and 2nd encoder sample extremums index */
    float phaseDiffMax;  /* phase difference clampping */
    
    /* - the following two parameters are our goal - */
    float encoderPhaseDiff = 0;
    uint8_t polePairNumber;
    
    /* first find the upper and lower boundaries of the sampled data */
    float min, max;
    findMinMax(encoder, sampleLength, &min, &max);
    
    /* this value is imported in order to resist noise,
     * just make sure STEP < thres < (max - min). */
    const float thres = (max - min) / 10.0f;
    
    /* find extremums using differentiation */
    derivativeCalcExtremum(thres, encoder, 0, sampleLength, &ext1);
    /* then we find the next extremum start from the first extermum plus half
     * of the electriangle round sample number. */
    derivativeCalcExtremum(thres, encoder, ext1 + halfRoundEA, sampleLength, &ext2);
    
    /* pole pair number = the sample number of the encoder used to rotate a
     * complete period / sample number of the electric angle to complete
     * a period. */
    polePairNumber = (int)(((float)(ext2 - ext1) / (float)roundEA) + 0.5f);
    /* obviously, the phase difference will never be greater than 360 / n. */
    phaseDiffMax = 360.0 / (float)polePairNumber;
    
    /* then we calculate encoderPhaseDiff */
    for(i = 0; i < sampleLength; i++)
    {
        /* calculate the rotor position and difference */
        float difference = encoder[i] - elecAngle[i] / 2.0f;
        
        /* normalize difference to 0 - phaseDiffMax. */
        while(difference > phaseDiffMax) difference -= phaseDiffMax;
        while(difference < 0.0f)         difference += phaseDiffMax;
        
        /* accumulation */
        encoderPhaseDiff += difference;
        
        /* average on each accumulation to avoid over flow. */
        encoderPhaseDiff /= 2.0f;
    }
    
    /* very hessian */
    evaluator->polePairNumber = polePairNumber;
    evaluator->phaseDiff      = encoderPhaseDiff;

    return evaluator;
}

/* calculate the rotor position in electric angle. range 0 - 360. */
float estimateElectricAngle(struct LinearEncoderEvaluator *evaluator,
                            const float encoderAngle)
{
    float position =
    (encoderAngle - evaluator->phaseDiff) * evaluator->polePairNumber;
    
    /* normalize the position to a 0 - 360 domain. */
    while(position > 360.0f) position -= 360.0f;
    while(position < 0.0f)   position += 360.0f;
    
    return position;
}

struct AbsoluteEncoderParameters*
estimateAbsoluteEncoderParas(const float *encoder, const uint32_t sampleLength,
                             const float lower,    const float upper,
                             float *INL,
                             struct AbsoluteEncoderParameters *calibrator)
{
    float min, max;
    
    assert(encoder && calibrator);
    assert(lower <= upper);
    
    findMinMax(encoder, sampleLength, &min, &max);
    
    calibrator->a = (upper - lower) / (max - min);
    calibrator->b = -min * calibrator->a + lower;
    
    if(INL) /* calculate INL */
    {
        uint32_t i;
        *INL = -FLT_MAX;
        
        for(i = 0; i < sampleLength; i++)
        {
            /* linear interpolation */
            float val = ((float)i / (float)sampleLength) * (upper - lower);
            float error =
            fabsf(calcuAbsoluteEncoderAngle(calibrator, encoder[i]) - val);
            
            /* max INL */
            if(error > *INL) *INL = error;
        }
    }
    
    return calibrator;
}

float calcuAbsoluteEncoderAngle(const struct AbsoluteEncoderParameters *encoderParas,
                                const float sampledValue)
{
    float angle = encoderParas->a * sampledValue + encoderParas->b;
    
    /* clampping */
    angle = angle < 0.0f   ? 0.0 : angle;
    angle = angle > 360.0f ? 360.0f : angle;
    
    return angle;
}
