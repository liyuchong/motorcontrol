/*
 * Copyright 2013, 2014 Li-Yuchong(李雨翀) <93@liyc.me>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR AND CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */

/* VVVF - Variable Voltage and Variable Frequency, 
 *                             THEORY OF OPERATION
 * ---------------------------------------------------------------------------
 * Here we have an unit circle representing the vector space of voltage,
 * described by the variable electricAngle. It will be updated in each system
 * tick interrupt. Inverse park transform will be applied, generating vAlpha
 * and vBeta. Then SVPWM will be applied to generate the final output.
 *
 * The speed of spin of the space vector was controlled by electricAngleStep,
 * since we have a constant rate of system tick interrupt. Vq, which controls
 * torque, was calculated according to electricAngleStep.
 */

#include "MotorControl.h"

#define _2PI 6.283185307179586                                        /* 2pi */

/* -------------- the two functions below are hardware related ------------- */
/*        Please implement them based on the processor you are using.        */
/* ------------------------------------------------------------------------- */
/* PWM comparator update function, there are 3 set of complementary outputs. */
extern void updatePWMComparator(uint32_t pwm1, uint32_t pwm2, uint32_t pwm3);
/* PWM reload register update function. */
extern void updatePWMReloadPeriod(uint32_t pwmReloadPeriod);
/* ------------------------------------------------------------------------- */

static float frequency;                  /* Hz, VVVF output frequency        */
static float electricAngle = 0.0;        /* electric angle, used by SVPWM    */

static float torqueVFCoeff;              /* torque coefficient               */
static float torqueCompensation;         /* torque compensation for low rpm  */
static float dcBusVoltage;               /* V, voltage of the DC bus         */
static float torqueMax;                  /* maximum torque could be achieved */

static float Vd, Vq;                     /* rotor magnatic flux and torque   */
static float electricAngleStep;
static float pwmReloadCoeff;             /* fixed reload value coefficient   */

static uint32_t pwmReloadPeriod;         /* PWM reload counter               */

static float systemTickCoeff;            /* step coeff in system tick        */
                                         /* schmitt trigger status           */
static bool schmittTriggerStatus[SCHMITT_TRIGGER_COUNT_MAX] = { false };

static enum ModulationMode modulationMode;
static uint32_t asyncModCarrierFreq;   /* async modulation carrier frequency */
static uint8_t carrierDomainTop = 0;   /* sub-sync mod arguments stack top   */
static uint32_t minimumCarrierFrequency;                     /* avoid jitter */

static uint32_t systemClock;           /* PWM related arguments              */
static uint32_t preScaler;

/* the sub-synchronous modulation frequency domain is described using the
 * following array. the even elements describe the critical value and
 * the odd elements describe the carrier frequency ratio(N).
 * the switch of range is buffered using schmitt trigger to avoid jitter.    */
float carrierFreqSubDomains[SCHMITT_TRIGGER_COUNT_MAX * 2];

/* carrier frequency update function */
static uint32_t calcCarrierFrequency(float fr);

void updateCarrierFrequency(float freq)
{
    /* calculate pwm reload register value */
    pwmReloadPeriod = (systemClock / calcCarrierFrequency(freq)) 
                    / (preScaler + 1);
    pwmReloadCoeff = pwmReloadPeriod / (float)SVPWM_PWMRELOAD_FIXED;

    updatePWMReloadPeriod(pwmReloadPeriod);
}

/* schmitt trigger to avoid carrier frequency jitter near critical zones. */
bool schmittTrigger(uint8_t number, float threshold, float offset, float value)
{
    if(schmittTriggerStatus[number])
    {
        if(value < threshold - offset)
            schmittTriggerStatus[number] = false;
    }
    else
    {
        if(value > threshold + offset)
            schmittTriggerStatus[number] = true;
    }
    
    return schmittTriggerStatus[number];
}

/* sub-synchronous modulation carrier frequency calculation
 * the modulation carrier frequency is splitted into domains
 * according to the output frequency fr. The carriar frequency
 * ratios are multiples of 3 which reduces even-order harmonics
 * in 3-phase systems. this will improve the stability of motor
 * control. */
uint32_t calcCarrierFrequency(float fr)
{
    uint32_t    carrierFreq;
    signed char domainIndex = -1;
    uint8_t     i;
    
    /* don't use sub-sync modulation */
    if(modulationMode == MOD_ASYNC)
        return asyncModCarrierFreq;
    
    /* examine all boundaries start with the highest frequency domain,
     * switch to the corresponding domain if the schmitt trigger flipped. */
    for(i = 0; i < carrierDomainTop; i++)
    {
        if(schmittTrigger(i, carrierFreqSubDomains[i << 1], SCHMITT_OFFSET, fr))
        {
            domainIndex = i;
            break;
        }
    }
    
    /* no schmitt trigger flipped */
    if(domainIndex == -1)
        carrierFreq = minimumCarrierFrequency;
    else
        carrierFreq = carrierFreqSubDomains[(domainIndex << 1) + 1] * fr;
    
    /* clamp to prevent jitter */
    if(carrierFreq < minimumCarrierFrequency)
        carrierFreq = minimumCarrierFrequency;
    
    return carrierFreq;
}

/* just use bubble sort */
static void rearrangeSubDomains(float *ptr)
{
    int i, j;
    for(i = 0; i < carrierDomainTop; i++)
        for(j = i; j < carrierDomainTop; j++)
            if(ptr[i << 1] < ptr[j << 1])
            {                
                float temp1       = ptr[ i << 1     ]; 
                float temp2       = ptr[(i << 1) + 1];
                ptr[ i << 1     ] = ptr[ j << 1     ];
                ptr[(i << 1) + 1] = ptr[(j << 1) + 1];
                ptr[ j << 1     ] = temp1; 
                ptr[(j << 1) + 1] = temp2;
            }
}

bool addCarrierFrequencySubDomain(float criticalFreq, uint32_t carrierRatio)
{
    /* not a multiple of 3, which will cause even-order harmonics. */
    if((carrierRatio % 3) != 0)
        fprintf(stderr, 
                "Warning: N is not a multiple of 3. "
                "in %s, %s(), LN: %d.\n", __FILE__, __FUNCTION__, __LINE__);

    if(carrierDomainTop == SCHMITT_TRIGGER_COUNT_MAX)
        return false; /* too many sub domains. */

    carrierFreqSubDomains[ carrierDomainTop << 1     ] = criticalFreq;
    carrierFreqSubDomains[(carrierDomainTop << 1) + 1] = carrierRatio;
    
    carrierDomainTop++;

    /* sort the sub domains according to critical frequencies. */
    rearrangeSubDomains(carrierFreqSubDomains);

    /* re-calculate minimum carrier frequency */
    minimumCarrierFrequency =
         (carrierFreqSubDomains[ (carrierDomainTop - 1) << 1] + SCHMITT_OFFSET)
        * carrierFreqSubDomains[((carrierDomainTop - 1) << 1) + 1];
    
    return true;
}

/* space vector pulse width modulation, optimized for fixed point processors. */
int SVPWM(uint32_t pwmPeriod,
          float voltage, float vAlpha, float vBeta,
          uint32_t *Taon, uint32_t *Tbon, uint32_t *Tcon)
{
    uint8_t  a, b, c, sector;
    int32_t  tempA, tempB, tempC;
    int32_t  t1, t2;
    int32_t  vAlphaI, vBetaI;
    uint32_t voltageI;
    
#define SQRT3 1.7320508075688772
    
    int32_t pwmHalf;                          /* pwmPeriod / 2    */
    const float sqrt3vAlpha = SQRT3 * vAlpha; /* sqrt(3) * vAlpha */
    
    /* float scale number */
#define _SCALE 2048
    
    /* scale the input decimal values to integer values */
    vAlphaI  = vAlpha  * _SCALE;
    vBetaI   = vBeta   * _SCALE;
    voltageI = voltage * _SCALE;
    
    /* fixed point scale bit */
#define _S_BIT 11

    pwmPeriod <<= _S_BIT;
    pwmHalf = pwmPeriod >> 1;
    
#undef _SCALE
    
#define SQRT3_FIXED       3547   /* sqrt(3) * _SCALE       */
#define HALF_SQRT3_FIXED  1774   /* (sqrt(3) / 2) * _SCALE */
    
    /* determine sector number */
    a = vBeta > 0.0 ? 1 : 0;
    b = ((-vBeta + sqrt3vAlpha) / 2.0) > 0.0 ? 1 : 0;
    c = ((-vBeta - sqrt3vAlpha) / 2.0) > 0.0 ? 1 : 0;
    
    /* inverse clarke transform */
#define _CALCULATE_X                                                           \
    ((((int32_t)SQRT3_FIXED * (int32_t)vBetaI)                                 \
    / (int32_t)voltageI) * (int32_t)pwmPeriod)
    
#define _CALCULATE_Y                                                           \
    ((((int32_t)vAlphaI * 3072 + (int32_t)vBetaI * (int32_t)HALF_SQRT3_FIXED)  \
    / (int32_t)voltageI) * (int32_t)pwmPeriod)
    
#define _CALCULATE_Z                                                           \
    ((((int32_t)-vAlphaI * 3072 + (int32_t)vBetaI * (int32_t)HALF_SQRT3_FIXED) \
    / (int32_t)voltageI) * (int32_t)pwmPeriod)
    
    /* calculate sector number */
    sector = (c << 2) + (b << 1) + a;
    switch(sector)
    {
        case 1: t1 =  _CALCULATE_Z; t2 =  _CALCULATE_Y; break;
        case 2: t1 =  _CALCULATE_Y; t2 = -_CALCULATE_X; break;
        case 3: t1 = -_CALCULATE_Z; t2 =  _CALCULATE_X; break;
        case 4: t1 = -_CALCULATE_X; t2 =  _CALCULATE_Z; break;
        case 5: t1 =  _CALCULATE_X; t2 = -_CALCULATE_Y; break;
        case 6: t1 = -_CALCULATE_Y; t2 = -_CALCULATE_Z; break;
        
        default: return 1; /* error */
    }

#undef SQRT3_FIXED
#undef HALF_SQRT3_FIXED
    
    t1 >>= _S_BIT;
    t2 >>= _S_BIT;
    
#undef _CALCULATE_X
#undef _CALCULATE_Y
#undef _CALCULATE_Z
    
    /* calculate null vector duty-time */
    tempA = ((pwmPeriod) - t1 - t2) >> 1;
    tempB = tempA + t1;
    tempC = tempB + t2;
    
    /* fit PWM duty-time according to sector number */
    switch(sector)
    {
        case 1: *Taon = tempB; *Tbon = tempA; *Tcon = tempC; break;
        case 2: *Taon = tempA; *Tbon = tempC; *Tcon = tempB; break;
        case 3: *Taon = tempA; *Tbon = tempB; *Tcon = tempC; break;
        case 4: *Taon = tempC; *Tbon = tempB; *Tcon = tempA; break;
        case 5: *Taon = tempC; *Tbon = tempA; *Tcon = tempB; break;
        case 6: *Taon = tempB; *Tbon = tempC; *Tcon = tempA; break;
        
        default: return 2; /* error */
    }

    /* linear transform */
    tempA = *Taon - pwmHalf; tempB = *Tbon - pwmHalf; tempC = *Tcon - pwmHalf;
    tempA *= SQRT3;          tempB *= SQRT3;          tempC *= SQRT3;
    *Taon = tempA + pwmHalf; *Tbon = tempB + pwmHalf; *Tcon = tempC + pwmHalf;
    
    /* shift back */
    *Taon >>= _S_BIT; *Tbon >>= _S_BIT; *Tcon >>= _S_BIT;
    
#undef SQRT3
#undef _S_BIT
    
    return 0; /* success */
}

void motorControlFrequencyChanged(uint8_t direction)
{
    if(direction == FREQUENCY_CHANGE_UP)
        frequency += FREQUENCY_UP_DOWN_STEP;
    else if(direction == FREQUENCY_CHANGE_DOWN)
        frequency -= FREQUENCY_UP_DOWN_STEP;
    
    /* boundaries */
    if(frequency > FREQUENCY_MAX) 
        frequency = FREQUENCY_MAX;
    if(frequency < FREQUENCY_MIN) 
        frequency = FREQUENCY_MIN;

    /* calculate step */
    electricAngleStep = systemTickCoeff * frequency;

    /* magnetic flux of the rotor = constant 0 */
    Vd = 0.0;
    /* rotor torque */
    Vq = torqueVFCoeff * fabsf(electricAngleStep) + torqueCompensation;

    /* VF with fixed V above rated voltage. */
    if(Vq > torqueMax) 
        Vq = torqueMax;

    /* update carrier frequency */
    updateCarrierFrequency(fabsf(frequency));
}

/* system tick interrupt */
void motorControlSystemTick(void)
{
    /* A, B, and C duty times */
    uint32_t a, b, c;
    /* voltage vector space coorindate */
    float x, y;
    float cosTheta, sinTheta;

    electricAngle += electricAngleStep;

    if(electricAngle > _2PI) electricAngle = 0.0;
    if(electricAngle < 0.0) electricAngle = _2PI;

    /* inverse park transform */
    /* Vα = Vd cosθ + Vq sinθ */
    /* Vβ = Vd sinθ + Vq cosθ */
    cosTheta = cosf(electricAngle);
    sinTheta = sinf(electricAngle);
    x = Vd * cosTheta + Vq * sinTheta;
    y = Vd * sinTheta + Vq * cosTheta;

    SVPWM(SVPWM_PWMRELOAD_FIXED, dcBusVoltage, x, y, &a, &b, &c);

    /* scale to actual reload register value */
    a *= pwmReloadCoeff; b *= pwmReloadCoeff; c *= pwmReloadCoeff;

    updatePWMComparator(a, b, c);
}

void motorControlInitializePWM(uint32_t clock, uint32_t psc)
{
    systemClock = clock;
    preScaler   = psc;
}

void motorControlInitializeVVVF(float busVoltage,     float ratedVoltage, 
                                float ratedFrequency, float torqueCompen,
                                enum ModulationMode mode,
                                uint32_t asyncModFreq)
{
    dcBusVoltage       = busVoltage;
    torqueMax          = busVoltage   / 3.0;
    torqueCompensation = torqueCompen / 3.0;

    modulationMode      = mode;
    asyncModCarrierFreq = asyncModFreq;

    /* since the variable electricAngle is accumulated in each tick, the
     * value of step is calculated like this: */
    /* step = 2pi / SYS_TICK_FREQ * frequency */
    systemTickCoeff = _2PI / SYS_TICK_FREQ;

    /* linear transform */
    /* match the torque compensation argument into torque space Vq. */
    torqueCompensation *= (ratedVoltage / busVoltage);

    torqueVFCoeff = ((ratedVoltage / 3.0) - torqueCompensation) 
                  / (systemTickCoeff * ratedFrequency);

    /* disable pwm to avoid accidental output */
    updatePWMComparator(0, 0, 0);
}

/* miscellaneous - getters & setters */

float getFrequency(void)
{
    return frequency;
}

void setFrequency(float freq)
{
    frequency = freq;
    motorControlFrequencyChanged(FREQUENCY_CHANGE_REFRESH);
}

void setAsyncModulationCarrierFrequency(float freq)
{
    asyncModCarrierFreq = freq;
    motorControlFrequencyChanged(FREQUENCY_CHANGE_REFRESH);
}

float getAsyncModulationCarrierFrequency(void)
{
    return asyncModCarrierFreq;
}

void setModulationMode(enum ModulationMode mode)
{
    modulationMode = mode;
    motorControlFrequencyChanged(FREQUENCY_CHANGE_REFRESH);
}

enum ModulationMode getModulationMode(void)
{
    return modulationMode;
}

#undef _2PI /* 2pi */
