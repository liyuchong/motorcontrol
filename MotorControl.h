/*
 * Copyright 2013, 2014 Li-Yuchong(李雨翀) <93@liyc.me>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR AND CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <stdio.h>
#include <math.h>

#include "Hardware.h"
#include "Types.h"

#ifndef __MOTOR_CONTROL_H__
#define __MOTOR_CONTROL_H__

#define SYS_TICK_FREQ          10000     /* Hz, system tick frequency        */
#define FREQUENCY_MAX          200.0     /* Hz, frequency boundaries         */
#define FREQUENCY_MIN         -200.0

#define FREQUENCY_UP_DOWN_STEP 0.1       /* Hz, frequency setter step        */

#define SCHMITT_OFFSET         0.5       /* schmitt trigger offset           */

/* args for motorControlFrequencyChanged() call */
#define FREQUENCY_CHANGE_UP       0
#define FREQUENCY_CHANGE_DOWN     1
#define FREQUENCY_CHANGE_REFRESH  2      /* re-calculate related parameters  */

#define SCHMITT_TRIGGER_COUNT_MAX 21

#define SVPWM_PWMRELOAD_FIXED     1024  /* use fixed register reload value in 
                                         * fixed point SVPWM algorithm to 
                                         * prevent overflow                  */

enum ModulationMode
{
    MOD_SUBSYNC, /* sub-sync modulation */
    MOD_ASYNC    /* async modulation    */
};

/* add a sub domain to the sub-sync modulation algorithm, notice that 
 * the argument carrierRatio(N) must be odd and a multiple of 3. */
bool addCarrierFrequencySubDomain(float criticalFreq, uint32_t carrierRatio);

/* motor control system VVVF parameters initialize */
void motorControlInitializeVVVF(float busVoltage,     float ratedVoltage,
                                float ratedFrequency, float torqueCompen,
                                enum ModulationMode mode, 
                                uint32_t asyncModFreq);

/* motor control system PWM parameters initialize */
void motorControlInitializePWM(uint32_t systemClock, uint32_t prescaler);

/* shuold be called when frequency setting is changed */
void motorControlFrequencyChanged(uint8_t direction);

/* system tick */
void motorControlSystemTick(void);

/* carrier frequency update function */
void updateCarrierFrequency(float frequency);

/* miscellaneous */
/* frequency getter & setter */
float getFrequency(void);
void  setFrequency(float freq);

void setModulationMode(enum ModulationMode mode);
enum ModulationMode getModulationMode(void);

void setAsyncModulationCarrierFrequency(float frequency);
float getAsyncModulationCarrierFrequency(void);

#endif /* __MOTOR_CONTROL_H__ */
